﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static IT_Support_Applet.SetWallpaper;

namespace IT_Support_Applet
{
    public partial class frmMain : Form
    {

        private Timer refreshTimer;
        private AppConfig config;
        private string configLocation;

        public frmMain(string configLocation)
        {
            this.configLocation = configLocation;
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            SetConfig();

            try
            {
                Directory.Delete(config.CreatedWallpapePath);
            } catch(Exception ex)
            {
                // Not too bothered if this fails
            }

            try
            {
                Directory.CreateDirectory(config.CreatedWallpapePath);
            } catch(Exception ex)
            {
                // Not too bothered about this right now...
            }

            GenerateAndSetWallpaper();

            var name = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            lblWelcome.Text = "Welcome, " + name;

            niNotificationIcon.DoubleClick += niNotificationIcon_DoubleClick;
            btnCreateSupportTicket.Text = config.SupportButtonTitle;

            lblSupportNumber.Text = config.SupportTel;

            CheckSystemUpTime();
            InitTimer();

            if(!config.OpenOnLogin.Equals("true"))
            {
                this.WindowState = FormWindowState.Minimized;
                this.ShowInTaskbar = false;
            }
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var url = "mailto:" + config.SupportEmail + "?body=" + GenerateWallpaper.GenerateSystemInformation();
            Process.Start(url);
        }

        private void niNotificationIcon_DoubleClick(object sender, EventArgs e)
        {
            Show();
            this.WindowState = FormWindowState.Normal;
            BringToFront();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        public void InitTimer()
        {
            refreshTimer = new Timer();
            refreshTimer.Tick += new EventHandler(refresh_TimerHandler);
            refreshTimer.Interval = Convert.ToInt32(config.RefreshIntervalMs); // 10 minutes in milliseconds
            refreshTimer.Start();
        }

        private void refresh_TimerHandler(object sender, EventArgs e)
        {
            GenerateAndSetWallpaper();
            CheckSystemUpTime();
        }

        private void GenerateAndSetWallpaper()
        {
            GenerateWallpaper.Generate(config);
            SetWallpaper.Set(new Uri(config.CreatedWallpapePath + @"\" + config.CreatedWallpaperFileName), Style.Stretched);
            config.CreatedWallpaperFileName = Guid.NewGuid().ToString() + ".jpg";
        }

        private void CheckSystemUpTime()
        {
            if(GenerateWallpaper.UpTime.Days >= config.SystemUpTimeCheckDays)
            {
                MessageBox.Show(config.RestartMessage, config.RestartTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void SetConfig()
        {
            string[] config = System.IO.File.ReadAllLines(configLocation);
            this.config = new AppConfig(config);
            this.config.CreatedWallpaperFileName = Guid.NewGuid().ToString() + ".jpg";
        }
    }
}
