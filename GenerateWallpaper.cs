﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace IT_Support_Applet
{
    class GenerateWallpaper
    {
        public static TimeSpan UpTime
        {
            get
            {
                using (var uptime = new PerformanceCounter("System", "System Up Time"))
                {
                    uptime.NextValue();       //Call this an extra time before reading its value
                    return TimeSpan.FromSeconds(uptime.NextValue());
                }
            }
        }

        public static void Generate(AppConfig config)
        {
            String username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String computerName = Environment.MachineName;

            var ipInfo = GetNetworkInformation();

            string ipAddress = "";
            string subnet = "";
            string gateway = "";
            string macs = "";

            foreach (string ip in ipInfo[0])
            {
                ipAddress += ip + "\t\t";
            }

            foreach (string sub in ipInfo[1])
            {
                subnet += sub + "\t\t";
            }


            foreach (string gate in ipInfo[2])
            {
                gateway += gate + "\t\t";
            }


            foreach (string mac in ipInfo[3])
            {
                macs += mac + "\t\t";
            }


            WriteInfoToBitmap(username, computerName, ipAddress, subnet, gateway, macs, config);
        }

        private static void WriteInfoToBitmap(string username, string computerName, string ipAddress, string subnet, string gateway, string macs, AppConfig config)
        {
            string imageFilePath = config.SourceWallpaper;
            Bitmap bitmap = (Bitmap)Image.FromFile(imageFilePath);

            var x = bitmap.Width / 2;
            var y = bitmap.Height / 2;
            PointF firstLocation = new PointF(x, y + 100f);
            PointF secondLocation = new PointF(x, y + 150f);
            PointF thirdLocation = new PointF(x, y + 200f);
            PointF fourthLocation = new PointF(x, y + 250f);
            PointF fifthLocation = new PointF(x, y + 300f);
            PointF sixthLocation = new PointF(x, y + 350f);
            PointF seventhLocation = new PointF(x, y + 400f);

            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                using (Font arialFont = new Font(config.DesktopFont, config.DesktopFontSize))
                {
                    graphics.DrawString("Username: " + username, arialFont, Brushes.White, firstLocation);
                    graphics.DrawString("Computer Name: " + computerName, arialFont, Brushes.White, secondLocation);
                    graphics.DrawString("System Uptime: " + FormatTimeSpan(UpTime), arialFont, Brushes.White, thirdLocation);

                    graphics.DrawString("IP Addresses: " + ipAddress, arialFont, Brushes.White, fourthLocation);
                    //graphics.DrawString("Subnet Masks: " + subnet, arialFont, Brushes.White, fifthLocation);
                    //graphics.DrawString("Gateways: " + gateway, arialFont, Brushes.White, sixthLocation);
                    //graphics.DrawString("Macs: " + macs, arialFont, Brushes.White, seventhLocation);
                }
            }

            bitmap.Save(config.CreatedWallpapePath + @"\" + config.CreatedWallpaperFileName);
            bitmap.Dispose();
        }

        private static string FormatTimeSpan(TimeSpan span)
        {
            string stringSpan = "";
            if (span.Days > 0)
            {
                stringSpan += span.Days + " Days ";
            }

            stringSpan += span.Hours + " Hours ";
            stringSpan += span.Minutes + " Minutes ";
            stringSpan += span.Seconds + " Seconds";

            return stringSpan;
        }

        private static List<List<string>> GetNetworkInformation()
        {
            List<string> ipAddresses = new List<string>();
            List<string> subnetMasks = new List<string>();
            List<string> macAddresses = new List<string>();
            List<string> gateways = new List<string>();

            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in adapters.Where(a => a.OperationalStatus == OperationalStatus.Up))
            {
                foreach (UnicastIPAddressInformation ip in adapter.GetIPProperties().UnicastAddresses)
                {
                    if (ip.Address.AddressFamily == AddressFamily.InterNetwork && !ip.Address.ToString().Equals("127.0.0.1"))
                    {
                        ipAddresses.Add(ip.Address.ToString());
                        subnetMasks.Add(ip.IPv4Mask.ToString());

                        if (adapter.GetIPProperties().GatewayAddresses.FirstOrDefault() != null)
                        {
                            gateways.Add(adapter.GetIPProperties().GatewayAddresses.FirstOrDefault().Address.ToString());
                        }
                        else
                        {
                            gateways.Add("Not set");
                        }
                        macAddresses.Add(adapter.GetPhysicalAddress().ToString());
                    }
                }
            }

            var info = new List<List<string>>();
            info.Add(ipAddresses);
            info.Add(subnetMasks);
            info.Add(gateways);
            info.Add(macAddresses);
            return info;
        }

        public static string GenerateSystemInformation()
        {
            String username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String computerName = Environment.MachineName;

            var ipInfo = GetNetworkInformation();

            string ipAddress = "";
            string subnet = "";
            string gateway = "";
            string macs = "";

            foreach (string ip in ipInfo[0])
            {
                ipAddress += ip + "\t\t";
            }

            foreach (string sub in ipInfo[1])
            {
                subnet += sub + "\t\t";
            }


            foreach (string gate in ipInfo[2])
            {
                gateway += gate + "\t\t";
            }


            foreach (string mac in ipInfo[3])
            {
                macs += mac + "\t\t";
            }

            return "==== SYSTEM INFORMATION ==== %0D%0A%0D%0A" + "Username: " + username + "%0D%0A" + "Computer Name: " + computerName + "%0D%0A" + FormatTimeSpan(UpTime) + "%0D%0A%0D%0A" + "= IP Info = %0D%0A" +  ipAddress + "%0D%0A" + subnet + "%0D%0A" + gateway + "%0D%0A" + macs + "%0D%0A%0D%0A ==== END ====";
        }
    }

}
