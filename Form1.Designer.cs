﻿
namespace IT_Support_Applet
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnCreateSupportTicket = new System.Windows.Forms.Button();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.niNotificationIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.lblSupportNumber = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCreateSupportTicket
            // 
            this.btnCreateSupportTicket.Location = new System.Drawing.Point(12, 45);
            this.btnCreateSupportTicket.Name = "btnCreateSupportTicket";
            this.btnCreateSupportTicket.Size = new System.Drawing.Size(238, 51);
            this.btnCreateSupportTicket.TabIndex = 0;
            this.btnCreateSupportTicket.Text = "Open a Support Ticket";
            this.btnCreateSupportTicket.UseVisualStyleBackColor = true;
            this.btnCreateSupportTicket.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Location = new System.Drawing.Point(13, 13);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(35, 13);
            this.lblWelcome.TabIndex = 1;
            this.lblWelcome.Text = "label1";
            // 
            // niNotificationIcon
            // 
            this.niNotificationIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("niNotificationIcon.Icon")));
            this.niNotificationIcon.Visible = true;
            // 
            // lblSupportNumber
            // 
            this.lblSupportNumber.AutoSize = true;
            this.lblSupportNumber.Location = new System.Drawing.Point(13, 115);
            this.lblSupportNumber.Name = "lblSupportNumber";
            this.lblSupportNumber.Size = new System.Drawing.Size(35, 13);
            this.lblSupportNumber.TabIndex = 2;
            this.lblSupportNumber.Text = "label1";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 137);
            this.Controls.Add(this.lblSupportNumber);
            this.Controls.Add(this.lblWelcome);
            this.Controls.Add(this.btnCreateSupportTicket);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "IT Support Applet";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreateSupportTicket;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.NotifyIcon niNotificationIcon;
        private System.Windows.Forms.Label lblSupportNumber;
    }
}

