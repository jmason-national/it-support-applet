﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IT_Support_Applet
{
    class AppConfig
    {
        public string SourceWallpaper;
        public double RefreshIntervalMs;
        public string RestartMessage;
        public string RestartTitle;
        public string SupportButtonTitle;
        public string CreatedWallpapePath;
        public string CreatedWallpaperFileName;
        public int SystemUpTimeCheckDays;
        public int DesktopFontSize;
        public string DesktopFont;
        public string SupportEmail;
        public string SupportTel;
        public string OpenOnLogin;

        public AppConfig(string[] configuration)
        {
            SourceWallpaper = configuration.Where(c => c.StartsWith("SourceWallpaper=")).FirstOrDefault()?.Replace("SourceWallpaper=", "");
            RefreshIntervalMs = Convert.ToDouble(configuration.Where(c => c.StartsWith("RefreshIntervalMs=")).FirstOrDefault()?.Replace("RefreshIntervalMs=", ""));
            RestartMessage = configuration.Where(c => c.StartsWith("RestartMessage=")).FirstOrDefault()?.Replace("RestartMessage=", "");
            RestartTitle = configuration.Where(c => c.StartsWith("RestartTitle=")).FirstOrDefault()?.Replace("RestartTitle=", "");
            SupportButtonTitle = configuration.Where(c => c.StartsWith("SupportButtonTitle=")).FirstOrDefault()?.Replace("SupportButtonTitle=", "");
            CreatedWallpapePath = configuration.Where(c => c.StartsWith("CreatedWallpapePath=")).FirstOrDefault()?.Replace("CreatedWallpapePath=", "");
            SystemUpTimeCheckDays = Convert.ToInt32(configuration.Where(c => c.StartsWith("SystemUpTimeCheckDays=")).FirstOrDefault()?.Replace("SystemUpTimeCheckDays=", ""));
            DesktopFontSize = Convert.ToInt32(configuration.Where(c => c.StartsWith("DesktopFontSize=")).FirstOrDefault()?.Replace("DesktopFontSize=", ""));
            DesktopFont = configuration.Where(c => c.StartsWith("DesktopFont=")).FirstOrDefault()?.Replace("DesktopFont=", "");
            SupportEmail = configuration.Where(c => c.StartsWith("SupportEmail=")).FirstOrDefault()?.Replace("SupportEmail=", "");
            SupportTel = configuration.Where(c => c.StartsWith("SupportTel=")).FirstOrDefault()?.Replace("SupportTel=", "");
            OpenOnLogin = configuration.Where(c => c.StartsWith("OpenOnLogin=")).FirstOrDefault()?.Replace("OpenOnLogin=", "");
        }
    }
}
